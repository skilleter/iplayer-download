# iplayer-download

A Python wrapper for the get-iplayer utility designed to run on Linux (it might, just might, run on MacOS, and please let me know if you manage it).

It uses a configuration file to determine programmes to download (or to skip) and where to load them. When it runs creates a log file and sends an email on completion.

Designed to be run daily from a crontab and, optionally, used in conjunction with my other tools; update-mp3-rss, folderise-videos and mp3-organiser.

This currently a bit of a lash-up and WILL require some customisation to run correctly in environments that aren't my server!

Future versions will, use the configuration file to allow more options to be customised and add other features on a when-I-get-around-to-it basis.

# Requirements

The `get-iplayer` utility must be installed on the path.

It may not be available as a package in your distro, but, if supported on your distro, can be installed as a snap.

If necessary, it can be cloned from `https://github.com/get-iplayer/get_iplayer.git` and a symlink created from `~/.local/bin/get-iplayer` to the copy of `get_iplayer` in the Git working tree.

Note that other Perl modules may be required for `get_iplayer` to run from the Git working tree and that the working tree should be updated whenever a new version of get_iplayer is released as the BBC have an occasional tendency to break things upon which it relies.

The `rtmpdump` utility should be installed. Note that the default version in earlier versions of Ubuntu (18.04 or earlier) may trigger out-of-memory conditions and you may have to replace it with a more up-to-date version.

# Command Line

        iplayer [-h] [-q] [-D] [-t] [-r] [-l] [-f] [-d] [-e] [-c CONFIG] [--dry-run] [--force] [--output-tv TV_OUTPUT_DIR] [--output-radio RADIO_OUTPUT_DIR] [--no-refresh] [--console] [--since SINCE] [regexp]

        Run get_iplayer to download TV and radio programmes

        optional positional arguments:
          regexp                Specify a regular expression matching programmes to download

        optional arguments:
          -h, --help            show this help message and exit
          -q, --quiet           Output minimal information to the console
          -D, --debug           Output debug information to the console
          -t, --tv              Download TV programmes
          -r, --radio           Download radio programmes
          -l, --list            Create programme listing files
          -f, --search          List matching programmes
          -d, --download        Download matching programmes (the default action unless list or find options are specified)
          -e, --email           Test email functionality
          -c CONFIG, --config CONFIG
                                Specify the configuration file (default: iplayer.ini)
          --dry-run             Simulate downloads (only when using the download option)
          --force               Download all matching programmes (default is to download only new programmes that have not previously been downloaded)
          --output-tv TV_OUTPUT_DIR
                                Directory to store tv programmes in (defaults to /server/video)
          --output-radio RADIO_OUTPUT_DIR
                                Directory to store radio programmes in (defaults to /server/audio)
          --no-refresh          Do not refresh the get_iplayer cache
          --console             Send log output to the console, instead of the log file
          --since SINCE         Only consider programmes made available after the specified number of hours in the past

# Defaults

The default output directories for downloaded media are:

* radio programmes: /server/audio
* TV programmes: /server/video

These can be overridden on the command line or in the configuration file.

By default, the CBeebies, S4C and Alba TV channels are ignored as are Radio 1, 5 & 6, Cymru, 1Xtra, Nan Gaidheal and the Asian Network - changing this involves modifying the EXCLUDE variable in the script.

# Configuration file

The script uses a configuration file called `iplayer.ini` which should be located in the directory where the script is run from.

The file is an Windows INI file (similar to TOML) and a simple example is below:

        [output]

        radio = /server/audio
        tv = /server/tv

        [email]
        recipient = wombat@gmail.com
        sender = cucumber@microsoft.com

        [defaults]
        all = News, Proms,
              Wombats
        tv = Blue Peter, Wombles
        radio = The Archers
        ignore = Dinosaurs

The `output` section specifies where TV and radio programmes should be downloaded to.

The `email` section defines the email account from which notification emails are sent (this must be configured for `sendmail` to use) and the account to which they are sent.

The `defaults` section defines a series of regular expressions which select programmes to be downloaded. The regexes are specifies as a set of comma-separated lists, which
can be broken over multiple lines, if the second and subsequent lines are indented as shown in the example above.

Each regex is matched against programme titles and descriptions and a programme is downloaded if it contains a match and has not previously been downloaded and does not match any of the entries in the ignore list.

Radio programmes are matched against entries in the `radio` and `all` lists and TV programmes are matched against entries in the `tv` and `all` lists.

So, the example above, any programme on TV containing a reference to 'Blue Peter', 'Wombles', 'News', 'Proms' or 'Wombats' will be downloaded so long as it doesn't also contain a reference to 'Dinosaurs'

The regex comparisons are performed case-independently and use partial matching against text in the programme name or description, so 'paper' would, for instance match against a programme called 'The Newspapers' or 'Newsnight' or a documentary on paper manufacturing.

# Caching

To avoid checking get-iplayer for each programme to see if it has already been downloaded, the script maintains a database in '~/.iplayer.pickle' of all programmes previously downloaded by the script.

If this is deleted then it will try and invoke get-iplayer for every programme that matches an entry in the iplayer.cfg file.

# Logging

When it runs, the script creates a log file named for the day of the week in a subdirectory called 'logfiles'. Any existing log file will be overwritten.

# Operation

The script is best run from the user's crontab file on a daily basis.

When it runs, it invokes get-iplayer to get a full list of all Tv and radio programmes matching the all/tv/radio configuration entries in the configuration file and excluding the 'ignore' entries.

Unless the force option has been specified, it then downloads only matching programmes which it hasn't previously downloaded (identified by the database saved in the home directory) or by get_iplayer itself.

It also sanitises the output filenames to make them safe for Windows or Linux filesystems.

Radio programmes are converted to MP3 format using ffmpeg.

TV programmes are not converted as they are downloaded and output by get-iplayer as MP4 files.

On completion an email is sent - NOTE that the script employs the sendmail utility to send the mail (sendmail must be installed as /usr/sbin/sendmail)

Note that downloads and logfiles are all created with umask 0 to facilitate sharing.
